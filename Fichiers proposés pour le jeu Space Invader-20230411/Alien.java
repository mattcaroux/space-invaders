import java.util.List;

import javafx.application.Platform;

import java.util.ArrayList;
import java.awt.Color;
import java.awt.Graphics2D;


public class Alien {
    private double posX;
    private double posY;
    private int tour;
    
    public Alien(double posX, double posY){
        this.posX = posX;
        this.posY = posY;
        this.tour = 0;
    }
    /**
    @return la position x de l'alien
    */
    public double getX(){
        return this.posX;
    }
    /**
    @return la position y de l'alien
    */
    public double getY(){
        return this.posY;
    }
    /** retourne l'ensemble de chaînes de caractères représentant l'alien avec ses caractéristiques visuelles
    @return (type) EnsembleChaines
    */
    public EnsembleChaines getEnsembleChaines(){
        EnsembleChaines a = new EnsembleChaines();
        if (tour % 5 == 0){
            a.ajouteChaine((int) posX, (int) posY -1, ("     ▀▄   ▄▀     "));
            a.ajouteChaine((int) posX, (int) posY -2, ("    ▄█▀███▀█▄    "));
            a.ajouteChaine((int) posX, (int) posY -3, ("   █▀███████ █   "));
            a.ajouteChaine((int) posX, (int) posY -4, ("   █ █▀▀▀▀▀█ █   "));
            a.ajouteChaine((int) posX, (int) posY -5, ("      ▀▀ ▀▀      "));
        }
        else{
            a.ajouteChaine((int) posX, (int) posY -1, ("   █ ▀▄   ▄▀ █   "));
            a.ajouteChaine((int) posX, (int) posY -2, ("   █▄█▀███▀█▄█   "));
            a.ajouteChaine((int) posX, (int) posY -3, ("    ▀███████     "));
            a.ajouteChaine((int) posX, (int) posY -4, ("     █▀▀▀▀▀█     "));
            a.ajouteChaine((int) posX, (int) posY -5, ("      ▀▀ ▀▀      "));
        }
        
        return a;
    }
    /**
     * Permet de faire bouger l'alien
     */
    public void evolue(){
        System.out.println(this.tour);
        if (this.tour < 100){
            this.posX += 0.1;
            getEnsembleChaines().majPositionX(this.posX);
        }
        if (this.tour == 100){
            this.posY -= 1;
            getEnsembleChaines().majPositionY(this.posY);
        }
        if(this.tour > 100){
            this.posX -= 0.1;
            getEnsembleChaines().majPositionX(posX);
        }
        if (this.tour == 200){
            this.tour = 0;
        }
        this.tour+=1;
    }
    /**
     * 
     * @param a
     * @param b
     * @return true si le point de coordonées est touché
     */
    public boolean contient(double a, double b) {
        return a >= this.posX && a < this.posX + 13 && b >= posY && b < posY + 5;
    }
    /**
     * Ferme l'application si il n'y a plus d'alien
     */
    public void mort(){
        if (getY() == 0){
            Platform.exit();
        }
    }
}