public class ChainePositionnee{
    double x,y;
    String c;
    public ChainePositionnee(int a,int b, String d){x=a; y=b; c=d;}
    public double getPositionX() {
        return x;
    }
    public void incrementeX(double nb){
        this.x+=nb;
    }
    public double getPositionY() {
        return y;
    }
    public void incrementeY(double nb){
        this.y+=nb;
    }

}
