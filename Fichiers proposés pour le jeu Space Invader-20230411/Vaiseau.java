public class Vaiseau {
    private double posX;

    public Vaiseau(){
    }
    public void deplace(double dx){
        if (posX + dx >= 0 && posX + dx <= 87){ //87 car 100 - 13 et 0 limite 
            posX += dx;
        }
    }
    public EnsembleChaines getEnsembleChaines(){
        EnsembleChaines V = new EnsembleChaines();
        V.ajouteChaine((int) posX, (int) 5.0, ("      ▄      "));
        V.ajouteChaine((int) posX, (int) 4.0, ("     ███     "));
        V.ajouteChaine((int) posX, (int) 3.0, ("▄███████████▄"));
        V.ajouteChaine((int) posX, (int) 2.0, ("█████████████"));
        V.ajouteChaine((int) posX, (int) 1.0, ("█████████████"));
        return V;
    }
    public double positionCanon(){
        return posX + 6;
    }
}