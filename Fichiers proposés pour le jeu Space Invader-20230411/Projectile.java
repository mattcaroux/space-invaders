public class Projectile {
    private double positionX, positionY;

    public Projectile(double positionX, double positionY){
        this.positionX = positionX;
        this.positionY = positionY;
    }
    /**
     * permet de retourner la position x
     * @return (type) double
     */
    public double getPosX(){
        return this.positionX;
    }
    /**
     * permet de retourner la position y
     * @return (type) double
     */
    public double getPosY(){
        return this.positionY;
    }
    /**
     * permet de prendre l'ensemble des chaines
     * @return (type) EnsembleChaines
     */
    public EnsembleChaines getEnsembleChaines(){
        EnsembleChaines N = new EnsembleChaines();
        N.ajouteChaine((int) positionX, (int) positionY,"▄");
        return N;
    }
    /**
     * Permet de faire bouger le projectile vers le haut
     */
    public void evolue(){
        positionY += 0.2;
    }
    @Override
    public String toString(){
        return "Projectile a la positions : (" + this.positionX + "," +this.positionY + ")";
    }
}