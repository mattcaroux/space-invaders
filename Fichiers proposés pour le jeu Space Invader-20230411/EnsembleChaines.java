import java.util.ArrayList;
public class EnsembleChaines {
    ArrayList<ChainePositionnee> chaines;
    public EnsembleChaines(){
        chaines = new ArrayList<ChainePositionnee>(); 
    }

    public void ajouteChaine(int x, int y, String c){
        chaines.add(new ChainePositionnee(x,y,c));
    }
    /**
     * Permet de mettre a jour la position de x
     * @param newPosX
     */
    public void majPositionX(double newPosX){
        for (ChainePositionnee chaine : chaines){
            double deplacementX = newPosX - chaine.getPositionX();
            chaine.incrementeX(deplacementX);
        }
    }
    /**
     * Permet de mettre a jour la position de y
     * @param newPosX
     */
    public void majPositionY(double newPosX){
        for (ChainePositionnee chaine : chaines){
            double deplacementY = newPosX - chaine.getPositionX();
            chaine.incrementeX(deplacementY);
        }
    }
    /**
     * Permet de faire l'union de toute les chaines
     * @param e
     */
    public void union(EnsembleChaines e){
        for(ChainePositionnee c : e.chaines)
            chaines.add(c);
    }
    /**
     * 
     * @param x
     * @param y
     * @return true si le projectile et l'alien ont la meme position
     */
    public boolean contient(int x, int y){
        for (ChainePositionnee chaine : chaines){
            if(x == chaine.getPositionX() && y == chaine.getPositionY()){
                return true;
            }
        }
        return false;
    }
}