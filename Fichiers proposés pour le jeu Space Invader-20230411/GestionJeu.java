import java.util.ArrayList;
import java.util.List;

import javafx.application.Platform;

public class GestionJeu {
    private int hauteur;
    private int largeur;
    private EnsembleChaines chaine;
    private EnsembleChaines chaineAlien;
    private int posX;
    private int posY;
    private int posXAlien;
    private int posYAlien;
    private Vaiseau vaisseau;
    private List<Alien> aliens;
    private List<Projectile> projectiles;
    

    public GestionJeu(){
        this.hauteur = 60;
        this.largeur = 100;
        this.vaisseau = new Vaiseau();
        this.aliens = new ArrayList<Alien>();
        Alien patrick = new Alien(15, 55);
        Alien jean = new Alien(30, 55);
        Alien pascal = new Alien(45, 55);
        Alien alfonse = new Alien(60, 55);
        Alien gngn = new Alien(15, 40);
        Alien lolita = new Alien(30, 40);
        Alien mister = new Alien(45, 40);
        Alien v = new Alien(60, 40);
        this.aliens.add(patrick);
        this.aliens.add(jean);
        this.aliens.add(pascal);
        this.aliens.add(alfonse);
        this.aliens.add(gngn);
        this.aliens.add(lolita);
        this.aliens.add(mister);
        this.aliens.add(v);
        this.chaine = new EnsembleChaines();
        this.posX = 0;
        this.posY = 30;
        projectiles = new ArrayList<>();
    }
    /**
     * Permet de retourner tous les aliens de la liste
     * @return (type) List<Alien>
     */
    public List<Alien> tousLesAliens(){
        return this.aliens;
    }
    /**
     * retourn la hauteur de la fenetre
     * @return type (int)
     */
    public int getHauteur(){
        return this.hauteur;
    }
    /**
     * retourne la largeur de la fenetre
     * @return type int
     */
    public int getLargeur(){
        return this.largeur;
    }
    /**
     * retourne la position en X
     * @return
     */
    public int getPosX(){
        return this.posX;
    }
    /**
     * retourne la position Y
     * @return (type) int
     */
    public int getPosY(){
        return this.posY;
    }
    /**
     * Retourne la chaine qui correspond a l'objet
     * @return (type) EnsembleChaines
     */
    public EnsembleChaines getChaines(){
        EnsembleChaines echaine = this.vaisseau.getEnsembleChaines();
        this.chaine.union(echaine);
        for(Projectile p : projectiles){
            echaine.union(p.getEnsembleChaines());
        }
        for (Alien a : aliens){
            echaine.union(a.getEnsembleChaines());
        }
        return echaine;
    }
    /**
     * Permet de deplacer le vaisseau de une case vers la gauche
     */
    public void toucheGauche(){
        vaisseau.deplace(-1);
        System.out.println("Appui sur la touche gauche");
    }
    /**
     * Permet de deplacer le vaisseau de une case vers la droite
     */
    public void toucheDroite(){
        vaisseau.deplace(+1);
        System.out.println("Appui sur la touche droite");
    }
    /**
     * Permet au vaisseau de tirer un projectile
     */
    public void toucheEspace(){
        Projectile p = new Projectile(vaisseau.positionCanon(), 5);
        projectiles.add(p);
        System.out.println("Appui sur la touche espace");
    }
    /**
     * Permet de savoir si l'on a touché un vaisseau
     */
    public void estTouchee(){
        List<Alien> lesAliens = new ArrayList<>();
        List<Projectile> lesProjectiles = new ArrayList<>();
        for (Alien a : aliens){
            for(Projectile p : projectiles){
                if(a.contient(p.getPosX(), p.getPosY())){
                    lesAliens.add(a);
                    lesProjectiles.add(p);
                    aliens.remove(a);
                    projectiles.remove(p);
                }
                if(aliens.size() == 0){
                    Platform.exit();
                }
            }
        }
    }
    /**
     * Permet de faire bouger les aliennes, de lancer les projectiles et de savoir si l'on a touché une cible
     */
    public void jouerUnTour(){
        for(Projectile p : projectiles){
            p.evolue();
        }
        for (Alien a : aliens){
            a.evolue();
        }
        estTouchee();
    }
}